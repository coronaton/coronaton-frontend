# coronaton-frontend (Digital Waitingroom)

## Table of Contents

- [Disclaimer](#disclaimer)<br>
- [The Project Group](#the-project-group) <br>
- [Setup](#setup)<br>
- [Available Scripts](#available-scripts)<br> -[yarn install](#yarn-install)<br> -[yarn start](#yarn-start)<br> -[yarn test](#yarn-test)<br> -[yarn build](#yarn-build)<br> -[yarn eject](#yarn-eject)<br> -[yarn add](#yarn-add)<br>
- [Credits](#credits)

## Disclaimer
This project was created as a part of the [WirVsVirus-Hackaton](https://wirvsvirushackathon.org), no work flow was 
enforced and therefore code quality is quite low.<br>
We deeply recommend to spend some time on implementing a linter (e.g. [typescript-eslint](https://github.com/typescript-eslint/typescript-eslint)).<br>
Further refactoring should take place, crating a clear folder structure and removing code duplication (there is a lot of it).<br>
Also yarn as well as npm were used in this project. we recommend to pick one and stay with it.

## The Project Group
This is the patient interaction frontend part of our hackaton project.<br>
It also consists of:<br>
- [a backend project](https://gitlab.com/coronaton/coronaton-backend) (containing the required API-Endpoints and a mongodb)<br>
- [a backoffice project](https://gitlab.com/coronaton/coronaton-backoffice) (containing the frontend for administration personal)<br>

## Setup

If you just cloned this project you have to run [`yarn/npm install`](#yarn-install).

## Available Scripts

In the project directory, you can run:

### `yarn install`

Install all the dependencies required by the project.<br>
The dependencies are listed within package.json.<br>

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.<br>
If an error occurs during buildtime, you have to eject using `ctrl + c` and restart the script.

### `yarn test`

Launches the jest test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `yarn add`

Adds a new package to the project.<br>
With `--dev` at the end of the call it will only be installed as a development dependency.<br>

## Credits
Icons used were created by [Just Icon](https://www.iconfinder.com/justicon) under the CC3 licence.