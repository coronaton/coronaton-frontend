import React, { useState } from 'react';
import { Redirect } from 'react-router';
import { useTranslation } from 'react-i18next';

interface IDoneProps {
}

interface IDoneState {
    redirect: boolean;
}

const DonePage = () => {
    const { t } = useTranslation();
    const [redirect, setRedirect] = useState(false);

    return (
        <div>
            {redirect === true ? <Redirect to='/' /> : null}
            <div>
                <button onClick={() => setRedirect(true)}>{t("done.back_to_home")}</button>
            </div>
        </div>
    );
}

export default DonePage;
