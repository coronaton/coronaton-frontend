import React, { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useSelector } from 'react-redux';
import {
    FormGroup,
    Button,
    Checkbox
} from '@material-ui/core'
import { useTranslation } from "react-i18next"
import { UserData } from '../../models/UserData';
import Store from '../../store/store';
import { setUser } from '../../store/actions';
import { Redirect } from 'react-router-dom';
import FormHeader from '../header/FormHeader'
import checkboxFieldsToUserData from './checkboxFieldsToUserData';
import valuesToDefaultFormData from './valuesToDefaultFormData';
import '../../Form.scss'

interface RootState {
    user: UserData
}

const possibleSymptoms: string[] = [
    'cough',
    'breathing',
    'fever',
    'sickness',
    'headache',
    'sore_troat',
    'diarrhea',
    'snuff',
    'limb_pain',
    'shivering',
    'poor_health'
];

const Form: React.FC = () => {
    const {t} = useTranslation();
    const [redirect, setRedirect] = useState("");
    const user = useSelector((state: RootState) => state.user);

    const userSymptoms = (user && user.profile && user.profile.symptoms) || [""]

    const { handleSubmit, control, getValues } = useForm({
        defaultValues: valuesToDefaultFormData(possibleSymptoms, userSymptoms)
    });

    useEffect(() => {
        const form = document.getElementsByTagName("form")[0];
        if (form) {
            form.scrollIntoView();
        }
    }, []);

    if (!user) {
        return <Redirect to="/form/1" />;
    }

    const mergeUserData = (values: any): UserData => {
        var updatedUser: UserData = {
            ...user,
            profile: {
                ...(user.profile || {}),
                symptoms: parseSymptoms(values)
            }
        };
        return updatedUser;
    }

    const parseSymptoms = (values: any): [string] => {
        return checkboxFieldsToUserData(possibleSymptoms, values);
    }

    const onSubmit = (values: any): any => {
        Store.dispatch(setUser(mergeUserData(values)));
        setRedirect("next");
    };

    const navigateBack = () => {
        const values = getValues();
        Store.dispatch(setUser(mergeUserData(values)));
        setRedirect("back");
    };

    switch (redirect) {
        case "next":
            return <Redirect to="/form/4" />;
        case "back":
            return <Redirect to="/form/2" />;
        default:
            return (
                <div>
                    <FormHeader/>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div>
                            <h5>
                                {t("symptoms")}
                            </h5>
                            <FormGroup className="checkbox-group">
                                {possibleSymptoms.map(symptom => (
                                    <div key={symptom}>
                                        <Controller
                                            as={Checkbox}
                                            control={control}
                                            id={symptom}
                                            name={symptom}
                                            onChange={(event: any) => {
                                                return event[1]
                                            }}
                                        />
                                        <label htmlFor={symptom}>
                                            {t(symptom)}
                                        </label>
                                    </div>
                                ))}
                            </FormGroup>
                        </div>
                        <div className="button">
                            <Button fullWidth onClick={navigateBack} >{t("back")}</Button>
                        </div>
                        <div className="button">
                            <Button fullWidth type="submit" >{t("continue")}</Button>
                        </div>
                    </form>
                </div>
            );
        }
};

export default Form;
