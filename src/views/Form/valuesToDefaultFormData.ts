export default (possibleValues: string[], userValues: [string]) => possibleValues.reduce(
    (acc: any, current: string) => ({ ...acc, [current]: Boolean(userValues.find(value => value === current)) }),
    {}
)
