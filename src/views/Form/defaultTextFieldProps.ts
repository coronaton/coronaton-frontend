const defaultTextFieldProps: { variant: 'outlined' } = {
    variant: "outlined"
};

export default defaultTextFieldProps;
