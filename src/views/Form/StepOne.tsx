import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { FormControl,
    Button,
    TextField
} from '@material-ui/core'
import { useTranslation } from "react-i18next"
import { UserData } from '../../models/UserData';
import Store from '../../store/store';
import { setUser } from '../../store/actions';
import { Redirect } from 'react-router-dom';
import FormHeader from '../header/FormHeader'
import defaultTextFieldProps from './defaultTextFieldProps';
import '../../Form.scss'

interface RootState {
    user: UserData
}

const Form: React.FC = () => {
    const {t} = useTranslation();
    const [redirect, setRedirect] = useState(false);
    const user = useSelector((state: RootState) => state.user || {
        forename: "",
        surname: ""
    });

    const { handleSubmit, control } = useForm({
        defaultValues: user
    });

    if (redirect) {
        return <Redirect to="/form/2" />
    }

    const onSubmit = (values: any): any => {
        var updatedUser: UserData = {
            ...user,
            forename: values.forename,
            surname: values.surname
        }

        Store.dispatch(setUser(updatedUser));
        setRedirect(true);
    };

    return (
        <div>
            <FormHeader/>
            <form onSubmit={handleSubmit(onSubmit)}>
                <h5>{t("name")}</h5>
                <div>
                    <FormControl fullWidth>
                        <Controller
                            control={control}
                            as={<TextField required/>}
                            name="forename"
                            id="forename"
                            label={t("forename")}
                            {...defaultTextFieldProps}
                        />
                    </FormControl>
                </div>

                <div>
                    <FormControl fullWidth>
                        <Controller
                            control={control}
                            as={<TextField required/>}
                            name="surname"
                            id="surname"
                            label={t("surname")}
                            {...defaultTextFieldProps}
                        />
                    </FormControl>
                </div>
                <div className="button">
                    <Button fullWidth type="submit" >{t("continue")}</Button>
                </div>
            </form>
        </div>
    );
};

export default Form;
