import React, { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { FormControl,
    Button,
    TextField
} from '@material-ui/core'
import { useTranslation } from "react-i18next"
import { UserData } from '../../models/UserData';
import Store from '../../store/store';
import { setUser } from '../../store/actions';
import { Redirect } from 'react-router-dom';
import FormHeader from '../header/FormHeader'
import defaultTextFieldProps from './defaultTextFieldProps';
import '../../Form.scss'

interface RootState {
    user: UserData
}

const Form: React.FC = () => {
    const {t} = useTranslation();
    const [redirect, setRedirect] = useState("");
    const user = useSelector((state: RootState) => state.user);

    const { handleSubmit, control, getValues } = useForm({
        defaultValues: {
            birthdate: (user && user.birthdate) || (new Date()).toISOString().split('T')[0]
        }
    });

    useEffect(() => {
        const form = document.getElementsByTagName("form")[0];
        if (form) {
            form.scrollIntoView();
        }
    }, []);

    if (!user) {
        return <Redirect to="/form/1" />;
    }

    const mergeUserData = (values: any): UserData => {
        var updatedUser: UserData = {
            ...user,
            birthdate: values.birthdate
        };
        return updatedUser;
    }

    const onSubmit = (values: any): any => {
        Store.dispatch(setUser(mergeUserData(values)));
        setRedirect("next");
    };

    const navigateBack = () => {
        const values = getValues();
        if (values.birthdate) {
            Store.dispatch(setUser(mergeUserData(values)));
        }
        setRedirect("back");
    };

    switch (redirect) {
        case "next":
            return <Redirect to="/form/3" />;
        case "back":
            return <Redirect to="/form/1" />;
        default:
            return (
                <div>
                    <FormHeader/>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div>
                            <FormControl fullWidth>
                                <Controller
                                    control={control}
                                    as={<TextField required/>}
                                    name="birthdate"
                                    id="birthdate"
                                    type="date"
                                    label={t("birthdate")}
                                    {...defaultTextFieldProps}
                                />
                            </FormControl>
                        </div>
                        <div className="button">
                            <Button fullWidth onClick={navigateBack} >{t("back")}</Button>
                        </div>
                        <div className="button">
                            <Button fullWidth type="submit" >{t("continue")}</Button>
                        </div>
                    </form>
                </div>
            );
    }
};

export default Form;
