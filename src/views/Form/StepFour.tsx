import React, { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useSelector } from 'react-redux';
import {
    FormControl,
    Button,
    FormGroup,
    Checkbox,
    RadioGroup,
    TextField,
    FormControlLabel,
    Radio
} from '@material-ui/core'
import { useTranslation } from "react-i18next"
import { UserData } from '../../models/UserData';
import Store from '../../store/store';
import { setUser } from '../../store/actions';
import { Redirect } from 'react-router-dom';
import FormHeader from '../header/FormHeader'
import defaultTextFieldProps from './defaultTextFieldProps';
import checkboxFieldsToUserData from './checkboxFieldsToUserData';
import valuesToDefaultFormData from './valuesToDefaultFormData';
import '../../Form.scss'

interface RootState {
    user: UserData
}

const possibleRisks = [
    'precondition_heart',
    'precondition_airways',
    'precondition_diabetes',
    'precondition_fat'
];

const possibleMedication = [
    'medication_cortisone',
    'medication_immunosuppressants'
];

const Form: React.FC = () => {
    const {t} = useTranslation();
    const [redirect, setRedirect] = useState("");
    const user = useSelector((state: RootState) => state.user);
    const profile: any = user && user.profile ? user.profile : {}

    const { handleSubmit, control, getValues } = useForm({
        defaultValues: {
            contact: (profile.contact && profile.contact.suspected.toString()) || '1',
            infected_contact: (profile.contact && profile.contact.confirmed.toString()) || '1',
            work_exposure: (profile.job && profile.job.exposure.toString()) || '1',
            work_importance: (profile.job && profile.job.importance.toString()) || '1',
            flatmates: profile.flatmates || '',
            ...valuesToDefaultFormData(possibleRisks, profile.risks || ['']),
            ...valuesToDefaultFormData(possibleMedication, profile.medication || ['']),
            travel: Boolean(profile.traveling && (profile.traveling.country || profile.traveling.region)),
            travel_country: (profile.traveling && profile.traveling.country) || '',
            travel_region: (profile.traveling && profile.traveling.region) || '',
            smoking: false,
            pregnant: false
        }
    });

    useEffect(() => {
        const form = document.getElementsByTagName("form")[0];
        if (form) {
            form.scrollIntoView();
        }
    }, []);

    if (!user) {
        return <Redirect to="/form/1" />;
    }

    const onSubmit = (values: any): any => {
        Store.dispatch(setUser(mergeUserData(values)));
        setRedirect("next");
    };

    const navigateBack = () => {
        const values = getValues();
        Store.dispatch(setUser(mergeUserData(values)));
        setRedirect("back");
    };

    const mergeUserData = (values: any): UserData => {
        var updatedUser: UserData = {
            ...user,
            profile: {
                ...(user.profile || {}),
                contact: {
                    suspected: Number(values.contact),
                    confirmed: Number(values.infected_contact)
                },
                job: {
                    exposure: Number(values.work_exposure),
                    importance: Number(values.work_importance),
                },
                risks: parseRisks(values),
                medication: parseMedication(values),
                traveling: parseTraveling(values),
                flatmates: values.flatmates ? Number(values.flatmates) : values.flatmates
            }
        };
        return updatedUser;
    }

    const parseRisks = (values: any): [string] => {
        const risks = checkboxFieldsToUserData(possibleRisks, values);
        if (values.smoking)
            risks.push("cough")
        if (values.pregnant)
            risks.push("breathing")

        return risks;
    }

    const parseMedication = (values: any): any => {
        return checkboxFieldsToUserData(possibleMedication, values);
    }

    const parseTraveling = (values: any): any => {
        var traveling: any = null

        if (values.travel){
            traveling={
                country: values.travel_country ,
                region: values.travel_region
            }
        }

        return traveling
    }

    switch (redirect) {
        case "next":
            return <Redirect to="/register" />;
        case "back":
            return <Redirect to="/form/3" />;
        default:
            return (
                <div>
                    <FormHeader/>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="radio-group">
                            <h5>{t("contact")}</h5>
                            <Controller
                                rules={{required: true}}
                                name="contact"
                                control={control}
                                as={
                                    <RadioGroup name="contact" aria-label={t("contact")}>
                                        <FormControlLabel
                                            value="1"
                                            control={<Radio/>}
                                            label={t("contact_none")}
                                        />
                                        <FormControlLabel
                                            value="2"
                                            control={<Radio/>}
                                            label={t("contact_second_grade")}
                                        />
                                        <FormControlLabel
                                            value="3"
                                            control={<Radio/>}
                                            label={t("contact_distant")}
                                        />
                                        <FormControlLabel
                                            value="4"
                                            control={<Radio/>}
                                            label={t("contact_high")}
                                        />
                                    </RadioGroup>
                                }
                            />
                        </div>

                        <div className="radio-group">
                            <h5>{t("infected_contact")}</h5>
                            <Controller
                                rules={{required: true}}
                                name="infected_contact"
                                control={control}
                                as={
                                    <RadioGroup name="infected_contact" aria-label={t("contact")}>
                                        <FormControlLabel
                                            value="1"
                                            control={<Radio/>}
                                            label={t("contact_none")}
                                        />
                                        <FormControlLabel
                                            value="2"
                                            control={<Radio/>}
                                            label={t("contact_second_grade")}
                                        />
                                        <FormControlLabel
                                            value="3"
                                            control={<Radio/>}
                                            label={t("contact_distant")}
                                        />
                                        <FormControlLabel
                                            value="4"
                                            control={<Radio/>}
                                            label={t("contact_high")}
                                        />
                                    </RadioGroup>
                                }
                            />
                        </div>

                        <div>
                            <h5>
                                {t("work")}
                            </h5>
                            <div className="radio-group">
                                <label>
                                    {t("work_exposure")}
                                </label>
                                <Controller
                                    rules={{required: true}}
                                    name="work_exposure"
                                    control={control}
                                    as={
                                        <RadioGroup name="work_exposure" aria-label={t("work_exposure")}>
                                            <FormControlLabel
                                                value="1"
                                                control={<Radio/>}
                                                label={t("work_exposure_low")}
                                            />
                                            <FormControlLabel
                                                value="2"
                                                control={<Radio/>}
                                                label={t("work_exposure_medium")}
                                            />
                                            <FormControlLabel
                                                value="3"
                                                control={<Radio/>}
                                                label={t("work_exposure_high")}
                                            />
                                        </RadioGroup>
                                    }
                                />
                            </div>
                            <div className="radio-group">
                                <label>
                                    {t("work_importance")}
                                </label>
                                <Controller
                                    as={
                                        <RadioGroup name="work_importance" aria-label={t("work_importance")}>
                                            <FormControlLabel
                                                value="1"
                                                control={<Radio/>}
                                                label={t("work_importance_low")}
                                            />
                                            <FormControlLabel
                                                value="2"
                                                control={<Radio/>}
                                                label={t("work_importance_medium")}
                                            />
                                            <FormControlLabel
                                                value="3"
                                                control={<Radio/>}
                                                label={t("work_importance_high")}
                                            />
                                        </RadioGroup>
                                    }
                                    rules={{required: true}}
                                    name="work_importance"
                                    control={control}
                                />
                            </div>
                        </div>

                        <div className="single-checkbox">
                            <FormGroup row>
                                <Controller
                                    as={Checkbox}
                                    control={control}
                                    id="smoking"
                                    name="smoking"
                                    onChange={(event: any) => {
                                        return event[1]
                                    }}
                                />
                                <label htmlFor="smoking">
                                    {t("smoking")}
                                </label>
                            </FormGroup>
                        </div>

                        <div className="single-checkbox">
                            <FormGroup row>
                                <Controller
                                    as={Checkbox}
                                    control={control}
                                    id="pregnant"
                                    name="pregnant"
                                    onChange={(event: any) => {
                                        return event[1]
                                    }}
                                />
                                <label htmlFor="pregnant">
                                    {t("pregnant")}
                                </label>
                            </FormGroup>
                        </div>

                        <div>
                            <FormGroup>
                                <div className="single-checkbox">
                                    <Controller
                                        as={Checkbox}
                                        control={control}
                                        id="travel"
                                        name="travel"
                                        onChange={(event: any) => {
                                            return event[1]
                                        }}
                                    />
                                    <label htmlFor="travel">
                                        {t("travel")}
                                    </label>
                                </div>
                                <FormControl>
                                    <Controller
                                        control={control}
                                        as={TextField}
                                        name="travel_country"
                                        id="travel_country"
                                        label={t("travel_country")}
                                        {...defaultTextFieldProps}
                                    />
                                </FormControl>
                                <FormControl>
                                    <Controller
                                        control={control}
                                        as={TextField}
                                        name="travel_region"
                                        id="travel_region"
                                        label={t("travel_region")}
                                        {...defaultTextFieldProps}
                                    />
                                </FormControl>
                            </FormGroup>
                        </div>

                        <div>
                            <h5>
                                {t("precondition")}
                            </h5>
                            <FormGroup className="checkbox-group">
                                {possibleRisks.map(risk => (
                                    <div className="column" key={risk}>
                                        <Controller
                                            as={Checkbox}
                                            control={control}
                                            id={risk}
                                            name={risk}
                                            onChange={(event: any) => {
                                                return event[1]
                                            }}
                                        />
                                        <label htmlFor={risk}>
                                            {t(risk)}
                                        </label>
                                    </div>
                                ))}
                            </FormGroup>
                        </div>

                        <div>
                            <h5>
                                {t("medication")}
                            </h5>
                            <FormGroup className="checkbox-group">
                                {possibleMedication.map(medication => (
                                    <div className="column" key={medication}>
                                        <Controller
                                            as={Checkbox}
                                            control={control}
                                            id={medication}
                                            name={medication}
                                            onChange={(event: any) => {
                                                return event[1]
                                            }}
                                        />
                                        <label htmlFor={medication}>
                                            {t(medication)}
                                        </label>
                                    </div>
                                ))}
                            </FormGroup>
                        </div>

                        <div>
                            <FormControl>
                                <Controller
                                    control={control}
                                    as={<TextField required/>}
                                    type="number"
                                    name="flatmates"
                                    id="flatmates"
                                    label={t("flatmates")}
                                    {...defaultTextFieldProps}
                                />
                            </FormControl>
                        </div>
                        <div className="button">
                            <Button fullWidth onClick={navigateBack} >{t("back")}</Button>
                        </div>
                        <div className="button">
                            <Button fullWidth type="submit" >{t("continue")}</Button>
                        </div>
                    </form>
                </div>
            );
    }
};

export default Form;
