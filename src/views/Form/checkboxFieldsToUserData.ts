export default (fields: string[], values: any): [string] => {
    var ret: any = [];
    for (const index in fields) {
        const field = fields[index];
        if (values[field]) {
            ret.push(field)
        }
    }
    return ret
}
