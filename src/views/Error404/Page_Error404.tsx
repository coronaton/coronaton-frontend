/*
 *  @author Vadym Melnyk
 *  @email vadym.melnyk@picard.de
 *  @create date 12.12.19, 09:57
 *  @modify date 02.12.19, 10:28
 */

import React from 'react';
import { Redirect } from 'react-router';

interface IProps {
}

interface IState {
    redirect?: boolean;
}

class Page_Error404 extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            redirect: false
        };
        this.BackToPath = this.BackToPath.bind(this);
    }

    BackToPath() {
        this.setState({
            redirect: true
        });
    }

    render() {
        return (
            <div>
                {this.state.redirect === true ? <Redirect to='/' /> : null}
                <div>
                    <h1>Error 404</h1>
                    <h2>Die Seite wurde nicht gefunden</h2>
                </div>
                <div>
                    <p>Die gewünschte Seite wurde leider nicht gefunden.</p>
                    <p>Bitte prüfen Sie Ihre Url auf einen Fehler und versuchen Sie es erneut.</p>
                    <button onClick={() => this.BackToPath()}>Zurück</button>
                </div>
            </div>
        );
    }
}

export default Page_Error404;