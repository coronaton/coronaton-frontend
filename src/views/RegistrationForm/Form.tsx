import React, { useState, useEffect } from "react";
import { useForm, Controller, ErrorMessage } from "react-hook-form";
import {
    FormControl,
    Button,
    RadioGroup,
    FormControlLabel,
    Radio,
    TextField
} from "@material-ui/core";
import { useTranslation } from "react-i18next"
import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import Store from '../../store/store';
import { setUser } from '../../store/actions';
import { UserData } from "../../models/UserData";
import { addUser } from "../../services/api/addUser"
import '../../Form.scss'
import FormHeader from "../header/FormHeader";

interface RootState {
    user: UserData
}

const defaultTextFieldProps: { defaultValue: string, variant: 'outlined' } = {
    defaultValue: "",
    variant: "outlined"
};

const Form: React.FC = () => {
    const user = useSelector((state: RootState) => state.user);
    const {t} = useTranslation();
    const [redirect, setRedirect] = useState("");

    const { handleSubmit, control, errors, getValues } = useForm({
        mode: 'onBlur',
        defaultValues: {
            phone: (user && user.phone) || '',
            email: (user && user.email) || '',
            mobility: (user && user.mobility) || 'VEHICLE',
            address: {
                street: (user && user.address && user.address.street) || '',
                number: (user && user.address && user.address.number) || '',
                zipcode: (user && user.address && user.address.zipcode) || ''
            }
        }
    });

    useEffect(() => {
        const header = document.getElementsByTagName("h5")[0];
        if (header) {
            header.scrollIntoView();
        }
    }, []);

    if (!user) {
        return <Redirect to="/form/1" />;
    }

    /*

    ADD INSURANCE -ID -NUMBER -NAME -PRIVATE_BOOLEAN

    */

    const onSubmit = async (values: any) => {
        var response = await addUser({ ...user, ...values})
        if (response.success) {
            setRedirect("next")
        } else {
            console.log("error")
        }
    };

    const navigateBack = () => {
        const values: any = getValues();
        Store.dispatch(setUser({...user, ...values}));
        setRedirect("back");
    };

    switch (redirect) {
        case "next":
            return <Redirect to="/done" />;
        case "back":
            return <Redirect to="/form/4" />;
        default:
            return (
                <div>
                    <FormHeader />
                    <h5>{t("registrationForm.registerHeadline")}</h5>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div>
                            <FormControl fullWidth>
                                <Controller
                                    rules={{
                                        required: true,
                                        pattern: {
                                            value: /^[0|+]\d+/i,
                                            message: t("registrationForm.invalidPhone")
                                        }
                                    }}
                                    control={control}
                                    as={<TextField required/>}
                                    name="phone"
                                    id="phone"
                                    label={t("registrationForm.phone")}
                                    {...defaultTextFieldProps}
                                />
                            </FormControl>
                            <ErrorMessage errors={errors} name="phone"/>
                        </div>
                        <div>
                            <FormControl fullWidth>
                                <Controller
                                    rules={{
                                        pattern: {
                                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                            message: t("registrationForm.invalidEmail")
                                        }
                                    }}
                                    control={control}
                                    as={TextField}
                                    name="email"
                                    id="email"
                                    label={t("registrationForm.email")}
                                    {...defaultTextFieldProps}
                                />
                            </FormControl>
                            <ErrorMessage errors={errors} name="email"/>
                        </div>
                        <div>
                            <FormControl fullWidth>
                                <Controller
                                    control={control}
                                    as={<TextField required/>}
                                    name="address.street"
                                    id="address.street"
                                    label={t("registrationForm.street")}
                                    {...defaultTextFieldProps}
                                />
                            </FormControl>
                        </div>
                        <div>
                            <FormControl fullWidth>
                                <Controller
                                    control={control}
                                    as={<TextField required/>}
                                    name="address.number"
                                    id="address.number"
                                    label={t("registrationForm.number")}
                                    {...defaultTextFieldProps}
                                />
                            </FormControl>
                        </div>
                        <div>
                            <FormControl fullWidth>
                                <Controller
                                    control={control}
                                    as={<TextField required/>}
                                    name="address.zipcode"
                                    id="address.zipcode"
                                    label={t("registrationForm.zipcode")}
                                    {...defaultTextFieldProps}
                                />
                            </FormControl>
                        </div>
                        <div className="radio-group">
                                <h5>{t("registrationForm.mobility")}</h5>
                            <FormControl required>
                                <Controller
                                    as={
                                    <RadioGroup name="mobility">
                                        <FormControlLabel
                                            value="VEHICLE"
                                            control={<Radio />}
                                            label={t("registrationForm.vehicle")}
                                        />
                                        <FormControlLabel
                                            value="PUBLIC_TRANSPORT"
                                            control={<Radio />}
                                            label={t("registrationForm.publicTransport")}
                                        />
                                        <FormControlLabel
                                            value="FOOT"
                                            control={<Radio />}
                                            label={t("registrationForm.foot")}
                                        />
                                    </RadioGroup>
                                    }
                                    rules={{ required: true }}
                                    name="mobility"
                                    control={control}
                                />
                            </FormControl>
                        </div>
                        <h5>
                            {t("registrationForm.stayAtHome")}
                        </h5>
                        <div className="button">
                            <Button fullWidth onClick={navigateBack} >{t("back")}</Button>
                        </div>
                        <div className="button">
                            <Button type="submit" >{t("registrationForm.register")}</Button>
                        </div>
                    </form>
                </div>
            );
    }
};

export default Form;
