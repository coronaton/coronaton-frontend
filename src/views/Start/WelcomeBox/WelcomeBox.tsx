/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-21 22:05:46 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-21 23:51:52
 */

import React from 'react';
import { ReactComponent as LandingVirusMobile } from './landing-virus-mobile.svg';
import { ReactComponent as LandingVirusDesktop } from './landing-virus-desktop.svg';
import { ReactComponent as LandingMedicDesktop } from './landing-medic-desktop.svg';
import { ReactComponent as Button1Mobile } from './button1-mobile.svg';
import { ReactComponent as Button2Mobile } from './button2-mobile.svg';
import './WelcomeBox.scss';
import { Trans } from 'react-i18next'

interface IProps {

}

interface IState {
    dimensions: {
        height: number,
        width: number
    }
}

export default class WelcomeBox extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            dimensions: {
                height: window.innerHeight,
                width: window.innerWidth
            }
        };

        this.updateDimensions = this.updateDimensions.bind(this);
    }
    
    updateDimensions = () => {
        this.setState({ dimensions : { width: window.innerWidth, height: window.innerHeight }});
    };
    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    anchorClick(e: React.MouseEvent<SVGSVGElement, MouseEvent>) {
        e.preventDefault()
        window.scroll({
            top: 100,
            behavior: 'smooth'
            });
    }

    render() {
       if (this.state.dimensions.width >= 1400) {
         return (
           <div className="welcomeBox">
                <div className="layerVirus-desktop">
                    <LandingVirusDesktop />
                </div>
               {/* <WelcomeDesktop /> */}
               <div className="welcomeTitle-desktop">
                    <div className="pageCovid-desktop">
                        <p>COVID-19</p>
                    </div>
                    <div className="pageTitle-desktop">
                        <p><Trans i18nKey='start.welcomebox.title' /></p>
                    </div>
                    <div className="pageTitleInfo-desktop">
                        <p><Trans i18nKey='start.welcomebox.subtitle' /></p>
                    </div>
               </div>
               <div className="layerMedic-desktop">
                    <LandingMedicDesktop />
                </div>
                {this.props.children}
           </div>
         );
       } else {
         return (
            <div className="welcomeBox">
                <div className="layerVirus-mobile">
                    <LandingVirusMobile />
                </div>
                {/* <WelcomeMobile  /> */}
                <div className="welcomeTitle-mobile">
                    <div className="pageCovid-mobile">
                        <p>COVID-19</p>
                    </div>
                    <div className="pageTitle-mobile">
                        <p><Trans i18nKey='start.welcomebox.title' /></p>
                    </div>
                    <div className="pageTitleInfo-mobile">
                        <p><Trans i18nKey='start.welcomebox.subtitle' /></p>
                    </div>
                </div>
                <div className="spacer"></div>
                <div className="buttonArea-mobile">
                    <Button1Mobile />
                    <Button2Mobile />
                </div>

                {this.props.children}
            </div>
         );
       }
    }
  
  }