import React from 'react';
import { ReactComponent as FooterMobile } from './footer-mobile.svg';
// import { ReactComponent as FooterDesktop } from './footer-desktop.svg';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import Button from '@material-ui/core/Button';
import './Footer.scss';
import { Trans } from 'react-i18next'

interface IProps {

}

interface IState {
    dimensions: {
        height: number,
        width: number
    }
}

export default class Footer extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            dimensions: {
                height: window.innerHeight,
                width: window.innerWidth
            }
        };

        this.updateDimensions = this.updateDimensions.bind(this);
    }
    
    updateDimensions = () => {
        this.setState({ dimensions : { width: window.innerWidth, height: window.innerHeight }});
    };
    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    render() {
       if (this.state.dimensions.width >= 1400) {
         return (
           <BottomNavigation className="Footer-desktop">
               {/* <FooterDesktop /> */}
               <Button><Trans i18nKey='start.menu.about' /></Button>
               <Button><Trans i18nKey='start.menu.dataprotection' /></Button>
               <Button><Trans i18nKey='start.menu.imprint' /></Button>
                {this.props.children}
           </BottomNavigation>
         );
       } else {
         return (
           <BottomNavigation className="Footer-mobile">
                <FooterMobile width={this.state.dimensions.width} height={this.state.dimensions.height} />
                {this.props.children}
           </BottomNavigation>
         );
       }
    }
  
  }