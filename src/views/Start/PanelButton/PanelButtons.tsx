/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-21 22:05:46 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-22 01:13:35
 */

import React from 'react';
import { ReactComponent as IconDoctor } from './doctor.svg';
import { ReactComponent as IconPatient } from './patient.svg';
import './PanelButtons.scss';
import Typography from '@material-ui/core/Typography';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { Trans } from 'react-i18next'

interface IProps {
    onLeft: (event: React.MouseEvent<HTMLDivElement>) => void,
    onRight: (event: React.MouseEvent<HTMLDivElement>) => void
}

interface IState {
    dimensions: {
        height: number,
        width: number
    }
}

export default class PanelButtons extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            dimensions: {
                height: window.innerHeight,
                width: window.innerWidth
            }
        };

        this.updateDimensions = this.updateDimensions.bind(this);
        this.onClickLeft = this.onClickLeft.bind(this);
        this.onClickRight = this.onClickRight.bind(this);
    }
    
    updateDimensions = () => {
        this.setState({ dimensions : { width: window.innerWidth, height: window.innerHeight }});
    };
    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    onClickLeft(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        e.preventDefault();
        if (this.props.onLeft !== null) {
            this.props.onLeft(e);
        }
    }

    onClickRight(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        e.preventDefault();
        if (this.props.onRight !== null) {
            this.props.onRight(e);
        }
    }

    render() {
       if (this.state.dimensions.width >= 1400) {
         return (
           <div className="PanelButtons-desktop">
               
               <div className="PanelButtonLeft-desktop noselect" onClick={this.onClickLeft}>
                    <div className="PBInfo-desktop">
                        <div className="PBInfoTitleTextLeft-desktop">
                            <Typography variant="h4" gutterBottom>
                                <Trans i18nKey='start.buttonselect.left.title' /><ArrowForwardIcon />
                            </Typography>
                        </div>
                        <div className="PBInfoTextLeft-desktop">
                            <Typography variant="body1" gutterBottom>
                                <Trans i18nKey='start.buttonselect.left.content' />
                            </Typography>
                        </div>
                    </div>
                    <IconDoctor className="PBIconLeft-desktop" />
               </div>
               <div className="PanelButtonRight-desktop noselect" onClick={this.onClickRight}>
                    <div className="PBInfo-desktop">
                        <div className="PBInfoTitleTextRight-desktop">
                            <Typography variant="h4" gutterBottom>
                                <Trans i18nKey='start.buttonselect.right.title' /><ArrowForwardIcon />
                            </Typography>
                        </div>
                        <div className="PBInfoTextRight-desktop">
                            <Typography variant="body1" gutterBottom>
                                <Trans i18nKey='start.buttonselect.right.content' />
                            </Typography>
                        </div>
                    </div>
                    <IconPatient className="PBIconRight-desktop" />
               </div>
                {this.props.children}
           </div>
         );
       } else {
         return (
            <div className="PanelButtons-mobile">
               
               <div className="PanelButtonLeft-mobile noselect" onClick={this.onClickLeft}>
                    <div className="PBInfo-mobile">
                        <div className="PBInfoTitleTextLeft-mobile">
                            <Typography variant="h4" gutterBottom>
                                <Trans i18nKey='start.buttonselect.left.title' /><ArrowForwardIcon />
                            </Typography>
                        </div>
                        <div className="PBInfoTextLeft-mobile">
                            <Typography variant="body1" gutterBottom>
                                <Trans i18nKey='start.buttonselect.left.content' />
                            </Typography>
                        </div>
                    </div>
                    <IconDoctor className="PBIconLeft-mobile" />
               </div>
               <div className="PanelButtonRight-mobile noselect" onClick={this.onClickRight}>
                    <div className="PBInfo-mobile">
                        <div className="PBInfoTitleTextRight-mobile">
                            <Typography variant="h4" gutterBottom>
                                <Trans i18nKey='start.buttonselect.right.title' /><ArrowForwardIcon />
                            </Typography>
                        </div>
                        <div className="PBInfoTextRight-mobile">
                            <Typography variant="body1" gutterBottom>
                                <Trans i18nKey='start.buttonselect.right.content' />
                            </Typography>
                        </div>
                    </div>
                    <IconPatient className="PBIconRight-mobile" />
               </div>
                {this.props.children}
           </div>
         );
       }
    }
  
  }