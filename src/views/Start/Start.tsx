/*
 * @Author: Tim Koepsel
 * @Date: 2020-03-21 00:12:37
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-22 05:41:17
 */

import React from 'react';
import Container from '@material-ui/core/Container';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import WelcomeBox from './WelcomeBox/WelcomeBox';
import Footer from './Footer/Footer';
import './Start.scss';
import InfoBox from './InfoBox/InfoBox';
import PanelButtons from './PanelButton/PanelButtons';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import { Redirect } from 'react-router-dom';
import { Trans } from 'react-i18next'

interface IProps {
}

interface IState {
    redirect: {
        shouldRedirect: boolean,
        redirectRoute: string
    }
}

class PageStart extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            redirect: {
                shouldRedirect: false,
                redirectRoute: ''
            }
        };

        this.onLeftButton = this.onLeftButton.bind(this);
        this.onRightButton = this.onRightButton.bind(this);
    }

    onLeftButton(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        this.setState({
            redirect: {
                shouldRedirect: true,
                redirectRoute: '/form/1'
            }
        })
    }

    onRightButton(e: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        this.setState({
            redirect: {
                shouldRedirect: true,
                redirectRoute: '/return'
            }
        })
    }

    render() {
        return (
            <div className="mainPage">
                {this.state.redirect.shouldRedirect === true ? <Redirect to={this.state.redirect.redirectRoute} /> : null}
                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    draggable
                    pauseOnHover
                />

                <Container>
                <AppBar className="topMenu" position="static">
                    <Toolbar Mui-desktop>
                        <MenuItem><Trans i18nKey='start.menu.about' /></MenuItem>
                        <MenuItem><Trans i18nKey='start.menu.dataprotection' /></MenuItem>
                        <MenuItem><Trans i18nKey='start.menu.imprint' /></MenuItem>
                    </Toolbar>
                    </AppBar>
                    <WelcomeBox />
                    <InfoBox />
                    <div className="spacer-small"></div>
                    <PanelButtons onLeft={this.onLeftButton} onRight={this.onRightButton} />
                    <Footer />
                </Container>
            </div>
          );
    }
}

export default PageStart;
