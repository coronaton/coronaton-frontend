/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-21 23:56:16 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-21 23:59:16
 */
import React from 'react';
import './InfoBox.scss';
import { ReactComponent as InfoBoxDesktop } from './infobox-desktop.svg';
import { ReactComponent as InfoBoxMobile } from './infobox-mobile.svg';

interface IProps {

}

interface IState {
    dimensions: {
        height: number,
        width: number
    }
}

export default class InfoBox extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            dimensions: {
                height: window.innerHeight,
                width: window.innerWidth
            }
        };

        this.updateDimensions = this.updateDimensions.bind(this);
    }
    
    updateDimensions = () => {
        this.setState({ dimensions : { width: window.innerWidth, height: window.innerHeight }});
    };
    componentDidMount() {
        window.addEventListener('resize', this.updateDimensions);
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    render() {
       if (this.state.dimensions.width >= 1400) {
         return (
           <div className="InfoBox-desktop">
                <InfoBoxDesktop />
                {this.props.children}
           </div>
         );
       } else {
         return (
            <div className="InfoBox-mobile">
                <InfoBoxMobile />
                {this.props.children}
            </div>
         );
       }
    }
  
  }