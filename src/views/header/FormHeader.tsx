import React from 'react';
import { useTranslation } from 'react-i18next';

import DoctorIllustration from './DoctorIllustration';
import './FormHeader.scss'

const FormHeader: React.FC = () => {
    const {t} = useTranslation();
    return (
        <div className="form-header">
            <h1>{t("form.header")}</h1>
            <p>
                {t("form.explanation")}
            </p>
            <div>
                <p className="speech-bubble">
                    {t("form.illustration")}
                </p>
                <DoctorIllustration />
            </div>
        </div>
    )
};

export default FormHeader;
