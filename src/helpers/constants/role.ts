export const Role = {
    SuperAdmin: 'ROLE_SUPERADMIN',
    Admin: 'ROLE_A_TRANSLATION',
    Moderator: 'ROLE_M_TRANSLATION'
}