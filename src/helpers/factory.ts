/*
 * @Author: Tim Koepsel 
 * @Date: 2019-12-12 13:56:18 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2019-12-12 13:56:41
 */

 class Factory {
    static create<T>(type: (new () => T)): T {
        return new type();
    }
 }

 export default Factory;