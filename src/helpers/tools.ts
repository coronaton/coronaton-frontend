/*
 * @Author: Tim Koepsel 
 * @Date: 2019-12-12 13:37:14 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2019-12-12 13:38:38
 */

 class Tools {
     static IsNullOrUndefined(val:any) {
         return (val === undefined || val === null);
     }
 }

 export default Tools;