class AsyncHelper {
    static AsyncPromiseBasedFunc() {
        return new Promise((resolve, reject) => {
            try {
                // ... hier passiert irgendwas, val ist das Ergebnis
                let val = true;
                resolve(val);
            } catch (error) {
                reject(error);
            }
        });
    }
}

export default AsyncHelper;