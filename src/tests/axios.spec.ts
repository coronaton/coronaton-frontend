/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-21 00:32:34 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-21 00:43:49
 */

import Api from "../services/api/api.service";

describe('api service', ()=> {
    it('does axios work', async ()=>{
        let response = await Api.get('https://feiertage-api.de/api/?jahr=2019');
        console.log(`http response status: ${response.status}`);
        expect(response.status).toBe(200); // should be 200
    });
});