import fetch from "node-fetch";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

describe('a test unit test collection', ()=> {
    it('calculating a number', ()=>{
        let a = 5;
        let b = 9;
        let sum = a+b;

        expect(sum).toBe(14);
    });
});
