import { UserData } from "../../models/UserData";

/** Dispatches */
export const setUser = (user?: UserData): { type: 'SET_USER', payload?: UserData } => {
    return {
        type: 'SET_USER',
        payload: user
    };
};

export default {
    setUser
};
