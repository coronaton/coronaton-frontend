import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationDE from "./locales/de/translation.json";

const resources = {
    "de-DE": {
        translation: translationDE
    }
}

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: "de-DE",
        keySeparator: false
    });

export default i18n