/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-21 02:05:23 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-21 02:20:49
 */

export interface Insurance {
    id: string,
    private: boolean,
    name: string
 }
