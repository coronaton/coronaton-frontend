/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-22 05:26:34 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-22 05:28:19
 */

export interface DataUser {
    firstname: string,
    lastname: string,
    birthdate: string,
    address: {
        street: string, 
        number: string,
        zip: string,
        city: string,
    }
    email: string,
    phone: string,
    profile: {
        symptoms: Array<string>
    }
}