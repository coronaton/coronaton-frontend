import { Insurance } from "./Insurance"

export interface UserData {
    forename: string,
    surname: string,
    birthdate?: string,
    address?:{
        street: string,
        number: string
        zipcode: string,
    },
    email?: string,
    phone?: string,
    profile?: {
        symptoms?: [string],
        mobility?: string,
        insurance?: Insurance
        contact?: {
            suspected: number,
            confirmed: number
        },
        job?:{
            exposure: number,
            importance: number,
        },
        risks?: [string],
        medication?: [string],
        traveling?: {
            country: string,
            region: string
        },
        flatmates?: number
    },
    mobility?: 'FOOT' | 'VEHICLE' | 'PUBLIC_TRANSPORT'
}
