/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-21 00:10:38 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-21 00:12:05
 */
 import axios from 'axios';

 const Api = axios.create({
    baseURL: 'http://127.0.0.1:5000'
  });
  
 export default Api;