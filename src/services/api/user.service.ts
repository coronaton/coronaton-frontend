/*
 * @Author: Tim Koepsel 
 * @Date: 2020-03-22 05:31:04 
 * @Last Modified by: Tim Koepsel
 * @Last Modified time: 2020-03-22 05:33:43
 */

import Api from "./api.service";
import { UserData } from "../../models/UserData";

export default class UserService {
    
    static RegisterUser(data: UserData) {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await Api.post('localhost:5000/users', data);
                if (response.status === 200) {
                    resolve(response.data);
                } else {
                    reject(response.statusText);
                }
            } catch (error) {
                reject(error);
            }
        });
    }
    static UpdateUser(userId: number, data: UserData) {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await Api.patch(`localhost:5000/users/${userId}`, data);
                if (response.status === 200) {
                    resolve(response.data);
                } else {
                    reject(response.statusText);
                }
            } catch (error) {
                reject(error);
            }
        });
    }
}
