import { UserData } from '../../models/UserData';
import Api from './api.service';

export const addUser = (user: UserData): Promise<{success: boolean, payload: any}> => {
    return Api.post(
       "/users/add",
       user
    ).then((response: any) => {
        return {success: true, payload: response}
    }).catch((error: any) => {
        return {success: false, payload: error}
    })
};

export default addUser