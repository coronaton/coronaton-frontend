import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Provider} from 'react-redux';
import {Route, Router, Switch} from 'react-router-dom'; // HashRouter as Router
import Store from './store/store';
import Page_Error404 from './views/Error404/Page_Error404';
import './i18n'
import {history} from './helpers/history';
import PageStart from './views/Start/Start';
import StepOne from './views/Form/StepOne';
import StepTwo from './views/Form/StepTwo';
import StepThree from './views/Form/StepThree';
import StepFour from './views/Form/StepFour';
import RegistrationForm from './views/RegistrationForm/Form';
import DonePage from './views/Done/Done'

const routing = (
    <Provider store={Store}>
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={PageStart} />

                <Route exact path="/form/1" component={StepOne} />

                <Route exact path="/form/2" component={StepTwo} />

                <Route exact path="/form/3" component={StepThree} />

                <Route exact path="/form/4" component={StepFour} />

                <Route exact path="/register" component={RegistrationForm} />

                <Route exact path="/done" component={DonePage} />

                <Route path='*' exact={true} component={Page_Error404} />
            </Switch>
        </Router>
    </Provider>
);

ReactDOM.render(routing, document.getElementById('root'));
